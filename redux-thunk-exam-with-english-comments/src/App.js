import React from 'react';
import {requestDog, requestDogSuccess, requestDogError} from './actions/actions'

//the dounloading function of image web-address from server
//(it returns the function of implementing specified process in which the component dispatch is being served)
const fetchDog = () => {
  return (dispatch) => {
    //the downloading of image web-address from server is being begining, 
    //the corresponding function we wrapping in to component dispatch
    dispatch(requestDog()); 
    fetch('https://dog.ceo/api/breeds/image/random')
    .then(res => res.json())
    .then (
      //the image web-address from server may be successfully finished, 
      //the corresponding function we wrapping in to component dispatch
      data => dispatch(requestDogSuccess(data)),
      //the downloading image web-address from server may by finished with error, 
      //the corresponding function we wrapping in to component dispatch 
      err => dispatch(requestDogError(err))
    )
  }
}

//Project component
class App extends React.Component {
  render () {
    return (
      <div>
        {/*we finding in props the component dispatch, and the function of image web-address downloading from server we wraping in it */}
        <button onClick={() => this.props.dispatch(fetchDog())}>Show Dog</button>
        {
          //from props we received the result about of specified proccess implementation
          this.props.loading
        ? 
          <p>Loading...</p>
        : 
          //from props we received the result about error, which arose at specified proccess implementation
          this.props.error 
        ? 
          <p>Error, try again</p> 
        :
          //we representing image, web-address of which was received in result of completed downloading
          <p><img alt="" src={this.props.url}/></p>
        }
      </div>      
    )
  }  
}

export default App;
