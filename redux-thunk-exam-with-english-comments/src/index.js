import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import {reducer} from './reducer/reducer'
import App from './App';

//we using thunk at creating the storage store
const store = createStore(reducer, applyMiddleware(thunk));

const ConnectedApp = connect((state) => {
  console.log(state);
  return state;
})(App)

ReactDOM.render(
  //we fixing the storage store in provider
  <Provider store={store}>
    <ConnectedApp />
  </Provider>,
  document.getElementById('root')
);