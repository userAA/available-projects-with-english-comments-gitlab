//an initial state of downloading proccess of image web-address from server
const initialState = 
{
    //image web-address on server
    url: '',
    //the flag of implementing proccess of image web-address downloading from server
    loading: false,
    //the flag, talking about, that during downloading proccess of image web-address from server an error has occured
    error: false
}

export const reducer = (state = initialState, action) => 
{
    switch (action.type) 
    {
        //the result of the process of launching the download of the web address of the image we puting into the storage store
        case 'REQUESTED_DOG': 
            return {
                url: '',
                loading: true,
                error: false
            };
        //the received web-adress of image action.url we putting into storage store
        case 'REQUESTED_DOG_SUCCEEDED':
            return {
                url: action.url,
                loading: false,
                error: false
            };
        //the message about error, arising in the ongoing proccess, we puting into storage store
        case 'REQUESTED_DOG_FAILED':
            return {
                url: '',
                loading: false,
                error: true
            }
        default:
            return state;
    }
}