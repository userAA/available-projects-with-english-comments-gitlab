//we begining the downloading proccess of image web-address from server
export const requestDog = () => 
{
    return {type: 'REQUESTED_DOG'}
};

//we carried out the downloading proccess of image web-address from server, received web-address we sending into store
export const requestDogSuccess = (data) => 
{
    return {type: 'REQUESTED_DOG_SUCCEEDED', url: data.message}    
};

//during of downloading proccess of image web-address from server, an error has occured
export const requestDogError = () => 
{
    return {type: 'REQUESTED_DOG_FAILED'}
};