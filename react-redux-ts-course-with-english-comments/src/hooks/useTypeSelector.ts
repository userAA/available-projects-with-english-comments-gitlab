import {TypedUseSelectorHook, useSelector} from "react-redux";

//we finding root state according to all reducer
import { RootState } from "../store/reducers";

//we compiling hook useTypedSelector using hook useSelector and root state according to all reducers
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;