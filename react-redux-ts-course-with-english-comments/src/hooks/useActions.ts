import { useDispatch } from "react-redux"
import { bindActionCreators } from "redux";

//we finding actions functions ActionCreators
import ActionCreators from '../store/action-creators/'

//we creating and exporting hook, which contains in itself the needed actions functions ActionCreators
export const useActions = () => {
    const dispatch = useDispatch();
    return bindActionCreators(ActionCreators, dispatch)
}