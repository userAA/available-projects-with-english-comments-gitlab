import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";

//we finding data storage store
import {store} from "./store";

ReactDOM.render(

  //we entering the data storage store into provider
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);