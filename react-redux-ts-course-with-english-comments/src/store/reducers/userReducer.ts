//we finding needed interfaces
import {UserAction,  UserState} from "../../types/user";

//we finding the labels list according to users
import {UserActionTypes} from "../../types/user";

//users names list state
const initialState: UserState = 
{
    //the list of user names itself
    users: [],
    //the downloading flag of users names list 
    loading: false,
    //the error, which occured at downloading of users names list
    error: null    
}

//we compiling and exporting the reducer according to users names list
export const userReducer = (state = initialState, action: UserAction): UserState => {
    switch (action.type) {
        //by tag UserActionTypes.FETCH_USERS the information about implementation of users names downloading we transfering into storage store
        case UserActionTypes.FETCH_USERS:
            return {loading: true, error: null, users: []}

        //by tag UserActionTypes.FETCH_USERS_SUCCESS the received information about downloaded users names we transfering into storage store
        case UserActionTypes.FETCH_USERS_SUCCESS:
            return {loading: false, error: null, users: action.payload}

        //by tag UserActionTypes.FETCH_USERS_ERROR the information about error which occured
        //during process of users names downloading we transfered into storage store
        case UserActionTypes.FETCH_USERS_ERROR:
            return {loading: false, error: action.payload, users: []}
        default:
            return state;
    }   
}