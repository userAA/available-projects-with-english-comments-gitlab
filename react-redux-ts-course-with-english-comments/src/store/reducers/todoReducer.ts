//we finding the required interfaces
import { TodoAction, TodoState } from "../../types/todo";

//we finding the required labels list
import { TodoActionTypes } from "../../types/todo";

//the initial state of tasks list
const initialState: TodoState = {
    //the list of task names itself
    todos: [],

    //the number of current page, on which we will show the downloaded tasks names
    page: 1,

    //the error which occured at carring out of tasks names downloading
    error: null,

    //the maximum quantity of downloaded tasks names, which we will show on page 
    limit: 10,

    //flag signaling the process of downloading task names
    loading: false
}

//we compiling and exporting the reducer according to tasks names list
export const todoReducer = (state = initialState, action: TodoAction): TodoState => {
    switch (action.type) {
        //by the tag TodoActionTypes.FETCH_TODOS the information about carring out of process of tasks names downloading
        //we transfering into storage store
        case TodoActionTypes.FETCH_TODOS:
            return {...state, loading: true}
        //by the tag  TodoActionTypes.FETCH_TODOS_SUCCESS the received information about downloaded tasks names
        //we transfering into storage store
        case TodoActionTypes.FETCH_TODOS_SUCCESS:
            return {...state, loading: false, todos: action.payload}
        //by the tag  TodoActionTypes.FETCH_TODOS_ERROR the information about error which occured
        //during process of tasks names downloading we transfering into storage store
        case TodoActionTypes.FETCH_TODOS_ERROR:
            return {...state, loading: false, error: action.payload} 
        //by the tag TodoActionTypes.FETCH_TODOS_ERROR, we transfer information about the page number   
        //on which we will show the downloaded task names to the store
        case TodoActionTypes.SET_TODO_PAGE:
            return {...state, page: action.payload}
        default:
            return state;
    }
}