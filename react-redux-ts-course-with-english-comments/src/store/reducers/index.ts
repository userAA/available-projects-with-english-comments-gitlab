import {combineReducers} from "redux";

//we finding reducer according to tasks names list
import { todoReducer } from "./todoReducer";

//we finding reducer according to users names
import {userReducer} from "./userReducer";

//we forming root reducer
export const rootReducer = combineReducers({
    //the reducer according to users we denoting as user
    user: userReducer,

    //the reducer according to tasks names list we denoting as todo
    todo: todoReducer
})

//we exporting the root state according to all reducers
export type RootState = ReturnType<typeof rootReducer>