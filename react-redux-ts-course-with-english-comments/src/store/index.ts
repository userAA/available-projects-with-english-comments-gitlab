import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";

//we finding root reducer rootReducer
import { rootReducer } from "./reducers";

//the storage store we forming with helping of root Reducer rootReducer and component thunk
export const store = createStore(rootReducer, applyMiddleware(thunk));