//we finding functions, related with users
import * as UserActionCreators from "./User";

//we finding functions, related with tasks names
import * as TodoActionCreators from "./todo";

//we exporting the functions default
export default 
{
    //related with tasks names
    ...TodoActionCreators,

    //related with users
    ...UserActionCreators
}