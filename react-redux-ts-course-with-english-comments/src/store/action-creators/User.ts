import axios from "axios"
import { Dispatch } from "react"

//we finding data interface into storage store, related with users names
import { UserAction } from "../../types/user"

//we finding the labels list related to users names
import { UserActionTypes } from "../../types/user"

//the exporting function of downloading users names from server
export const fetchUsers = () => {
    return async (dispatch: Dispatch<UserAction>) => {
        try 
        {
            //we fixing the signal about downloading users names from server
            dispatch({type: UserActionTypes.FETCH_USERS})

            //we making up request for downloading users names from server
            const response = await axios.get('https://jsonplaceholder.typicode.com/users');
            setTimeout(() => {
                //we fixing result of specified downloading and according to label UserActionTypes.FETCH_USERS_SUCCESS
                //we transfering it into storage store via corresponding reducer 
                dispatch({type: UserActionTypes.FETCH_USERS_SUCCESS, payload: response.data}); 
            }, 500);
        } 
        catch (e) 
        {
            //according to label TodoActionTypes.FETCH_TODOS_ERROR into storage store via corresponding reducer
            //we sending the message about error, which occured at downloading of users names from server
            dispatch({
                type: UserActionTypes.FETCH_USERS_ERROR, 
                payload: 'The error occured at downloading of users names'
            })
        }
    }
}