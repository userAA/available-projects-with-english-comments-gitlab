import axios from "axios"
import { Dispatch } from "react"

//we finding the data interface into storage store, related with tasks names list
import { TodoAction } from "../../types/todo"

//we finding the labels list related to tasks names list
import {  TodoActionTypes } from "../../types/todo"

//the exporting function of tasks names list downloading from server accoeding to page with number page
//at maximum quantity of tasks names limit
export const fetchTodos = (page:number = 1, limit:number = 10) => 
{
    return async (dispatch: Dispatch<TodoAction>) => 
    {
        try 
        {
            //we fixing signal of tasks names list downloading with server
            dispatch({type: TodoActionTypes.FETCH_TODOS})

            //we making request for downloading of tasks names list from server
            const response = await axios.get('https://jsonplaceholder.typicode.com/todos',
            {
                //in specified request we sending the page _page of tasks names and maximum quantity
                //of corresponding names on page _limit
                params: {_page: page, _limit: limit}
            }
            );
            setTimeout(() => 
            {
                //the downloaded tasks names list we sending according to label TodoActionTypes.FETCH_TODOS_SUCCESS into storage store
                //via corresponding reducer
                dispatch({type: TodoActionTypes.FETCH_TODOS_SUCCESS, payload: response.data}); 
            }, 500);
        } 
        catch (e) 
        {
            //according to label TodoActionTypes.FETCH_TODOS_ERROR via corresponding reducer into storage store
            //we sending the message about error, which occured at downloading of tasks names list from server 
            dispatch(
            {
                type: TodoActionTypes.FETCH_TODOS_ERROR, 
                payload: 'The error occured at downloading of tasks names list'
            })
        }
    }
}

//we exporting the setting function current page number, on which we will show the tasks names
export function setTodoPage(page: number): TodoAction {
    return {
        type: TodoActionTypes.SET_TODO_PAGE,
        payload: page
    }
}