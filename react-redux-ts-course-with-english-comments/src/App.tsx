import React from "react";

//we finding the list of tasks names
import TodoList from "./components/TodoList";

//we finding the list of users
import UserList from "./components/UserList";

const App = () => {
  return (
    <div>
      {/*We representing the list of users */}
      <UserList/>
      <hr/>
      {/*We representing the list of tasks names */}
      <TodoList/>
    </div>
  );
};

export default App;
