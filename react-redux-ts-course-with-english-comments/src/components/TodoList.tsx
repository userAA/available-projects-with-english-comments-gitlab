
//we finding hook useEffect
import React, { useEffect } from "react";
//we finding formed actions hook useActions
import { useActions } from "../hooks/useActions";
//we finding formed hook useTypedSelector
import { useTypedSelector } from "../hooks/useTypeSelector";

//tasks names list component
const TodoList: React.FC = () => 
{
    //using formed hook useTypedSelector from storage store we finding the page of representing of tasks names,
    //the message about error which arose at downloading of tasks names,
    //flag that signals the process of downloading the list of task names
    //the list of task names itself
    //the maximum naber of tasks names on pages
    const {page, error, loading, todos, limit} = useTypedSelector(state => state.todo)

    //from hook of actions we finding the functions related with list of tasks names
    const {fetchTodos, setTodoPage} = useActions()

    //we set an array of page numbers by which we will distribute the list of tasks
    const pages = [1,2,3,4,5];

    useEffect(() => {
        //in the beginning we downloading with server the list of tasks names in accordance with number of current page
        //and maximum quantity of tasks names limit
        fetchTodos(page,limit)
    },[page, fetchTodos, limit])

    //the downloading of tasks names list from server is being implemented
    if (loading) 
    {
        return <h1>The download is in progress...</h1>
    }

    //an error occurred when downloading the list of task names from the server
    if (error) 
    {
        return <h1>{error}</h1>
    }

    return (
        <div>
            {/*We representing the tasks names list itself in accordance with number of current page */}
            {todos.map(todo =>
                <div key={todo.id}>{todo.id} - {todo.title}</div>
            )}
            {/*We representing the pagination according to which we will show the tasks names list */}
            {/*And we representing the possibility of changing tasks names list according to changing of number of current page page */}
            <div style={{display: "flex"}}>
                {pages.map(p => 
                    <div 
                        onClick={() => setTodoPage(p)}
                        style={{border:p === page? '2px solid green' : '1px solid gray', padding: 10}}
                    >
                        {p}
                    </div>
                )}
            </div>
        </div>
    )
}

export default TodoList;