//we finding hook useEffect
import React, { useEffect } from "react";
//we finding formed hook  useTypedSelector
import { useTypedSelector } from "../hooks/useTypeSelector";
//we finding formed hook of actions useActions
import {useActions} from "../hooks/useActions";

//users names component
const UserList: React.FC = () => {
    //using hook useTypedSelector from storage store we finding the users names list users
    //flag that signals the process of downloading user names from the server
    //and message about error which may occur at implementation of downloading of users names from server
    const {users,error,loading} = useTypedSelector(state => state.user);

    //we finding thr request function for downloading of users names from server
    const {fetchUsers} = useActions();

    useEffect(() => {
        //we activating function fetchUsers
        fetchUsers()
    },[fetchUsers])

    //we signaling process of downloading users names from server
    if (loading) 
    {
        return <h1>The download is in progress...</h1>
    }

    //the event when error occured at downloading of users names from server
    if (error) {
        return <h1>{error}</h1>
    }
    
    return (
        <div>
            {/*We outputing all downloaded users names from server */}
            {users.map(user =>
                <div key={user.id}>{user.name}</div>
            )}
        </div>
    )
}

export default UserList;