//initial state interface according to users names
export interface UserState 
{
    //users names list
    users: any[];
    //flag for loading a list of user names
    loading: boolean;
    //the error, which occured at downloading of users names list
    error: null | string;
}

//we compiling the labels list according to users names and exporting it
export enum UserActionTypes 
{
    //the label of the users names downloading process
    FETCH_USERS = "FETCH_USERS",
    //the label of the successfully completed user downloading names process
    FETCH_USERS_SUCCESS = "FETCH_USERS_SUCCESS",
    //the error label, which occured at carrying out the process of downloading users names
    FETCH_USERS_ERROR = "FETCH_USERS_ERROR"
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_USERS
interface FetchUsersAction {
    type: UserActionTypes.FETCH_USERS;
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_USERS_SUCCESS
interface FetchUsersSuccessAction {
    type: UserActionTypes.FETCH_USERS_SUCCESS;
    payload: any[]
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_USERS_ERROR
interface FetchUsersErrorAction {
    type: UserActionTypes.FETCH_USERS_ERROR;
    payload: string;
}

//we exporting data interfaces, which are being transfered into storage store via reducer
export type UserAction = FetchUsersAction | FetchUsersErrorAction | FetchUsersSuccessAction;