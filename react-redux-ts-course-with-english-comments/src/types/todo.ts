//the initial state interface according to tasks names list
export interface TodoState {
    //tasks names list itself
    todos: any[];

    //flag for the process of downloading the list of task names
    loading: boolean;

    //the error which occured at implementation of tasks names downloading
    error: null | string;

    //the current page number, on which we will show the downloaded tasks names
    page: number;

    //the maximum quantity of downloaded tasks names, which we will show on page 
    limit: number;
}


export enum TodoActionTypes {
    //the label of the start of the process of downloading the names of tasks from the server
    FETCH_TODOS = "FETCH_TODOS",
    //the label of successfull finishing of downloading process of new tasks names from server
    FETCH_TODOS_SUCCESS = "FETCH_TODOS_SUCCESS",
    //the label of the error that occurred during the process of downloading the names of tasks from the server
    FETCH_TODOS_ERROR = "FETCH_TODOS_ERROR",
    //the label of definition page on which we will show the downloaded tasks from server
    SET_TODO_PAGE = 'SET_TODO_PAGE'
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_TODOS 
interface FetchTodoAction {
    type: TodoActionTypes.FETCH_TODOS
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_TODOS_SUCCESS
interface FetchTodoSuccessAction {
    type: TodoActionTypes.FETCH_TODOS_SUCCESS;
    payload: any[];
}

//data interface which are being transfered into storage store via corresponding reducer according to label FETCH_TODOS_ERROR
interface FetchTodoErrorAction {
    type: TodoActionTypes.FETCH_TODOS_ERROR;
    payload: string;
}

//data interface which are being transfered into storage store via corresponding reducer according to label SET_TODO_PAGE
interface SetTodoPage {
    type: TodoActionTypes.SET_TODO_PAGE;
    payload: number;
}

//exporting data interfaces, which is being transfered into storage store via reducer 
export type TodoAction = FetchTodoAction | FetchTodoErrorAction | FetchTodoSuccessAction | SetTodoPage;