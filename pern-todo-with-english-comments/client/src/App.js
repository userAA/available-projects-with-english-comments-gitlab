import React, {Fragment} from "react";
import './App.css';

//we finding the component for creating of new task name
import InputTodo from "./components/InputTodo";
//we finding the component for representing of list of new tasks names
import ListTodos from "./components/ListTodos";

function App() {
  return (
    <Fragment>
      <div className="container">
        <InputTodo/>
        <ListTodos/>
      </div>
    </Fragment>
  );
}

export default App;
