import React, {Fragment, useState} from "react";

//the compiling component of name new task
const InputTodo = () => {
    //the compiling of name new task
    const [description, setDescription] = useState("");

    //the function for sending of created name of new task into server to corresponding database
    const onSubmitForm = async e => 
    {
        e.preventDefault();
        try 
        {
            //the compiled name of the new task
            const body = {description};
            //the POST request for sending of created name of new task into server to corresponding database
            const response = await fetch("http://localhost:5000/todos", 
            {
                method: "POST",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(body)
            });
            window.location="/";  
            //we clearing the field settings of new task name
            document.getElementById("textInput").value = "";
        } 
        catch (err) 
        {
            //the specified POST request did not pass
            console.error(err.message);
        }
    }

    return (
        <Fragment>
            <h1 className="text-center mt-5">Pern Todo List</h1>
            <form className="d-flex" onSubmit={onSubmitForm}>
                {/*Task field of the name of the new task */}
                <input
                    id="textInput" 
                    type="text" 
                    className="form-control" 
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                />
                {/*The button for adding of name of new task into server to corresponding database*/}
                <button className="btn btn-success">Add</button>
            </form>
        </Fragment>
    )
}

export default InputTodo;