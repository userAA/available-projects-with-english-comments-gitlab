import React, {Fragment, useState} from "react";

//the component for edditing name of separate new task
const EditTodo = ({todo}) => {
    //the state of edditing name of new task
    const [description, setDescription] = useState(todo.description);

    //the edditing function of new task name 
    const updateDescription = async(e) => {
        e.preventDefault();
        try 
        {
            //we finding the eddited new task name
            const body = {description};
            //we defining the ID id of eddited new task name
            const id = todo.id;
            //we implementing PUT request into server for edditing in it te name of new task with ID id
            const response = await fetch(`http://localhost:5000/todos/${id}`, {
                method: "PUT",
                headers: {"Content-Type": "application/json"},
                body: JSON.stringify(body)
            })

            window.location = "/";
        } 
        catch (err) 
        {
            //the specified request failed
            console.error(err.message)            
        }
    }

    return (
        <Fragment>
            {"The button for opening of modal window for edditing new task "}
            <button 
                type="button" 
                className="btn btn-warning" 
                data-toggle="modal" 
                data-target={`#id${todo.id}`}
            >
                Edit
            </button>

            {/*Modal window itself*/}
            <div 
                className="modal" 
                id={`id${todo.id}`}
                onClick={() => setDescription(todo.description)}
            >
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h4 className="modal-title">Edit Todo</h4>
                            {/*Cross button for modal window closing, at the same time the corresponding 
                               name of new task is not being changed*/}
                            <button 
                                type="button" 
                                className="close" 
                                data-dismiss="modal" 
                                onClick={() => setDescription(todo.description)}
                            >
                                &times;
                            </button>
                        </div>

                        <div className="modal-body">
                            {/*The definition field of eddited name of new task*/}
                            <input 
                                type="text" 
                                className="form-control" 
                                value={description} 
                                onChange={e => setDescription(e.target.value)}
                            />
                        </div>

                        <div className="modal-footer">
                            {/*the fixing button of eddited name of new task*/}
                            <button 
                                type="button" 
                                className="btn btn-warning" 
                                data-dismiss="modal"
                                onClick={e => updateDescription(e)}
                            >
                                Edit
                            </button>
                            {/*Modal window close button, at the same time the corresponding name of new task is not being changed*/}
                            <button 
                                type="button" 
                                className="btn btn-danger" 
                                data-dismiss="modal"
                                onClick={() => setDescription(todo.description)}
                            >
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    )
}

export default EditTodo;