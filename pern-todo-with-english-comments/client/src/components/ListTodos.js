import React, {Fragment, useEffect, useState} from "react";

//we finding the modal window for creating of name of new task
import EditTodo from "./EditTodo";

//the component of list of names of new tasks, downloaded with server
const ListTodos = () => {
    //the state of list of names of new tasks
    const [todos, setTodos] = useState([]);

    //the request function for removing of name of new task according to ID id from corresponding database on server
    const deleteTodo = async (id) => 
    {
        try 
        {
            //we creating request for removing of name of new task according to ID id from corresponding database on server 
            const deleteTodo = await fetch(`http://localhost:5000/todos/${id}`, 
            {
                method: "DELETE"
            });
            //we updating the list of names of new tasks, removing from it the specified name of new task
            setTodos(todos.filter(todo => todo.id !== id));
        } 
        catch (err) 
        {
            //the spesified request did not pass
            console.error(err.message);
        }
    }

    //the request function for downloading of full list of names of new tasks with server
    const getTodos = async () => 
    {
        try 
        {
            //we creating request for downloading of full list of names of new tasks with server
            const response = await fetch("http://localhost:5000/todos");
            //we fixing the result of specified request implementing
            const jsonData = await response.json();        
            setTodos(jsonData.allTodos);
        } 
        catch (err) 
        {
            //the specified request did not pass
            console.error(err.message);
        }
    }

    useEffect(() => 
    {
        //first of all from server we requesting the entire list of names of new tasks, if there is
        getTodos();
    }, []);

    return (
        <Fragment>
            {"The table of names of new tasks "}
            <table className="table mt-5 text-center">
                {"Title of the table of names of new tasks "}
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {"The names of new tasks themeselves "}
                    {todos && todos.map(todo => {
                        return (
                            <tr key={todo.id}>
                                {"The very name of a separate new task"}
                                <td>{todo.description}</td>
                                {"The modal window for edditing name of new task"}
                                <td>
                                    <EditTodo todo={todo} />
                                </td>
                                <td>
                                    {"The remove button of name of new task from server and from corresponding list todos"}
                                    <button 
                                        className="btn btn-danger" 
                                        onClick={() => deleteTodo(todo.id)}
                                    >
                                        Delete
                                    </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </Fragment>
    )
};

export default ListTodos;