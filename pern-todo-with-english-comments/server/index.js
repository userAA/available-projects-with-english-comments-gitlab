require('dotenv').config();
const express = require("express");
//setting the database
const sequelize = require('./db');
//we defining component cors using which we will send the needed requests from client-application
const cors = require("cors");

//we finding the database model of tasks names
const {Todo} = require('./model');

const PORT = process.env.PORT || 5000;

const app = express();
app.use(cors());
app.use(express.json());

//ROUTERS//

//the request router for adding to database of tasks names the name of new task
app.post("/todos", async(req,res) => 
{
    try 
    {
        //we defining the name itself of new task
        const {description} = req.body;
        //we implementing the specified addition
        const newTodo = Todo.create({description: description});
        res.json("SUCCESS");
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//the request router for receiving of all tasks names from corresponding database
app.get("/todos", async (req,res) => 
{
    try
    {
        //we defining all task s names in database Todo
        const allTodos = await Todo.findAll();
        //we sending the received result into client-application
        res.json({allTodos: allTodos});
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//the request router for changing of task name in corresponding database with defined ID
app.put("/todos/:id", async (req, res) => 
{
    try 
    {
        //we receiving the ID of the name of the task that is being changed
        const {id} = req.params;
        //we receiving the final version of the name of the task that is being changed
        const {description} = req.body;
        //we implementing the specified changing
        await Todo.update({description: description}, {where: {id: id}});
        res.json("Todo was updated!")
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//the request router for removing of task name with known ID from corresponding database
app.delete("/todos/:id", async (req, res) => 
{
    try 
    {
        //defining the identifier of the task name to be deleted
        const {id} = req.params;
        //we implementing the specified removing
        await Todo.destroy({
            where: {
                id: id
            }
        })
        res.json("Todo was deleted!");
    } 
    catch (err) 
    {
        console.log(err.message);
    }
})

//the function of connecting to the postgress database and starting the server
const start = async () => {
    try 
    {
        //we starting up database postgress
        await sequelize.authenticate();
        await sequelize.sync();
        //we listening server
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    }
    catch (e) 
    {
        console.log(e);
    }
} 

//we activating the function of connection to database postgress and starting the server 
start();