const sequelize = require('./db');
const {DataTypes} = require('sequelize');

//the database model of tasks names
const Todo = sequelize.define('todo', 
{
    //the ID of task name
    id : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //the task name itself
    description: {type: DataTypes.STRING}
})

//we exporting the database model of tasks names
module.exports = 
{
    Todo
}