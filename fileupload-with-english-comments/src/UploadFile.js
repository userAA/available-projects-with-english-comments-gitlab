import axios from 'axios';
import React, {Component} from 'react'
import './App.css';

//files loading component
export class UploadFile extends Component {
    constructor(props) {
        super(props)

        //the initial state of all loaded files (it empty)
        this.state = {
            //the name of every loading file
            selectedFiles:null,
            //tracker of the process of uploading each file to the server
            progress:'',
            //the name of the resulting web address of each file on the server
            urls:[],
            //loading flag of each file into server
            uploaded:false
        }
    }
    //fixing function of state of loaded files names into selectedFiles
    fileHandler = (event) => 
    {
        //we fixing the names of loading files
        this.setState({selectedFiles: event.target.files});
    }
    //loading function of all files
    fileUpload=()=>{
        let files=[];
        for (let i=0; i<this.state.selectedFiles.length; i++) 
        {
            //we creating data for implementation post-request for file loading into server cloudinary
            let formData = new FormData();
            formData.append('file',this.state.selectedFiles[i]);
            formData.append('upload_preset','ogdn0qdq')
            //we making up post-request for file loading into server cloudinary
            axios.post('https://api.cloudinary.com/v1_1/df3ugt9dd/image/upload',formData,
            {
                //we tracking the specified loading into server cloudinary
                onUploadProgress:(progressEvent) => 
                {
                    this.setState({progress: Math.round(progressEvent.loaded/progressEvent.total*100)+"%"});
                }
            })
            .then(response => 
            {
                //the loading was implemented successfully
                files.push(response.data.url);
                //we forming the array od web-addresses of loaded into server cloudinary of specified files
                let urls=[...this.state.urls];
                urls.push(response.data.url);
                this.setState({urls})
                if (files.length === this.state.selectedFiles.length)
                {
                    //all files were loaded successfully into server
                    this.setState({uploaded:true})
                    //the entering field of files names loaded from disk into server cloudinary we clearing
                    this.fileInput.value="";
                    //tracker progress of loading of specified files into server cloudinary we returning into initial state 
                    setTimeout(() => 
                    {
                        this.setState({uploaded:false, progress: ''})
                    }, 3000)
                }
            })
        }
    }
    render() {
        return (
            <div>
                <form className="fileupload">
                    {/*Input field for the number of uploaded files */}
                    <input 
                        type="file" 
                        ref={(ref) => (this.fileInput=ref)} 
                        multiple 
                        className="inputfile" 
                        onChange={this.fileHandler}
                    />
                    {/*The loading button of all loaded files from disk into server cloudinary */}
                    <input 
                        type="button" 
                        onClick={this.fileUpload} 
                        className="submit" 
                        //we controlling the loading proccess of all specified files into server cloudinary
                        value={this.state.progress ? 'Uploading..' + this.state.progress:'Upload'}
                    />
                </form>
                {/*we fixing moment, when specified loading was implemented successfully */}
                {this.state.uploaded && <h1 style={{textAlign: "center", color:"white"}}>Uploaded Successfully</h1>}
                <div style={{textAlign:"center"}}>
                    {this.state.urls.length > 0 && this.state.urls.map(url => (
                        //we showing every loaded file from disk according to it formed web-address on server cloudinary
                        <img key={url} src={url} alt="Cloudinary pic"/>
                    ))}
                </div>
            </div>
        )
    }
}
