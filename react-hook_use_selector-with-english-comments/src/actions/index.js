//we finding the functions of actions, related with counter
import counterActions from "./counterActions";

//we finding the functions of actions, realted with current user
import userActions from "./userActions";

//we exporting all actions
const allActions = 
{
    counterActions,
    userActions
}

export default allActions;