//the request for authorization of current user
const setUser = (userObj) => {
    return {
        //the request label for authorization of current user
        type: "SET_USER",
        //the information about authorized user
        payload: userObj
    }
}

//the request for canceling of authorization with current user
const logOut = () => {
    return {
        //the label request for canceling of authorization with current user, if he was authorized
        type: "LOG_OUT"
    }
}

//we exporting the functions of compiled requests
export default {
    setUser,
    logOut
}