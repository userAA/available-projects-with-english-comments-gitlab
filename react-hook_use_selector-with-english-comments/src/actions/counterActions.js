//the request for increasing of counter value
const increment = () => {
    return {
        type: "INCREMENT"
    }
}

//the request for decreasing of counter value
const decrement = () => {
    return {
        type: "DECREMENT"
    }
}

//we exporting the compiled requests functions, which is related from counter
export default {
    increment,
    decrement
}