//we defining the counters values
const counter = (state = 1, action) => {
    switch (action.type) {
        //the increased on unit the value of counter we sending into storage store
        case "INCREMENT":            
            return state+1; 
        //the decreased on unit the value of counter we sending into storage store  
        case "DECREMENT":
            return state-1;
        default:
            return state;
    }
}

//the new information about counter state we sending into storage store
export default counter;