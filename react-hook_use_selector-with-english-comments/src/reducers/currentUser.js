//we compiling the information about current user
const currentUser = (state = {}, action) => {
    switch (action.type) {
        //we compiling the information about current user, which we transfering into storage store when he is authorized
        case "SET_USER":
            return {
                ...state,
                //received information about current user
                user: action.payload,
                //the authorization flag of current user is positive
                loggedIn: true
            }
        //from storage store we removing the information about current user at canceling of his authorization
        case "LOG_OUT":
            return {
                ...state,
                //there is no information about current user
                user: {},
                //the authorization flag of current user is negative
                loggedIn: false
            }
        default:
            return state;
    }
}

//the compiled information about current user according to specified labels we sending into storage store
export default currentUser;