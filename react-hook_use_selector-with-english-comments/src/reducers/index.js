//we finding reduxer according to current user
import currentUser from './currentUser';

//we finding reducer according to counter
import counter from './counter';
import {combineReducers} from 'redux';

//we compiling root reducer
const rootReducer = combineReducers({
    //the reducer according to current user
    currentUser,
    //the reduser according to counter
    counter
})

export default rootReducer;