import React, {useEffect} from 'react';
//вытаскиваем хуки useSelector и useDispatch
//we finding hook useSelector and useDispatch
import {useSelector, useDispatch} from 'react-redux';
import './App.css';
//вытаскиваем все функции запросов связанных с счетчиком и с текущим пользователем
//we finding all requests functions related with counter and with current user
import allActions from './actions';

//страница иллюстрации хука useSelector
//the page of hook useSelector representation
const App = () => {
  //информацию о счетчике вытаскиваем с помощью хука useSelector  
  //the information about counter we finding using hook useSelector  
  const counter = useSelector(state => state.counter);
  //информацию о текущем пользователе вытаскиваем с помощью хука useSelector  
  //the information about current user we finding using hook useSElector
  const currentUser = useSelector(state => state.currentUser);

  //вытаскиваем компонент dispatch из хука useDispatch
  //we finding the component dispatch from hook useDispatch
  const dispatch = useDispatch();

  //задаем имя текущего пользователя
  //we setting the name of the current user
  const user = {name: "Rei"};

  useEffect(() => {
    //прежде всего, проводим авторизацию текущего пользователя
    //first of all, we carring out the authorization of current user
    dispatch(allActions.userActions.setUser(user))
  }, [])

  return (
    <div className="App">
      {
        //случай когда текущий пользователь авторизован
        //the event, when current user is authorized
        currentUser.loggedIn ?
        <>
          {/*Выводим имя текущего пользователя */}
          {/*We outputing the name current user */}
          <h1>Hello, {currentUser.user.name}</h1>
          {/*Кнопка снятия авторизации с текущего пользователя */}
          {/*The button of canceling authorization from current user */}
          <button onClick={() => dispatch(allActions.userActions.logOut())}>Logout</button>
        </>
        :
        //случай когда текущий пользователь авторизован
        //the event when current user is authorized
        <>
          <h1>Login</h1>
          Х
          {/*Кнопка запуска процесса по авторизации текущего пользователя */}
          {/*The proccess start button for authorization of current user */}
          <button onClick={() => dispatch(allActions.userActions.setUser(user))}>Login as Rei</button>
        </>
      }
      {/*Текущее значение счетчика */}
      {/*Current counter value */}
      <h1>Counter: {counter}</h1>
      {/*Кнопка увеличения значения счетчика на единицу */}
      {/*Button to increase the counter value by unit */}
      <button onClick={() => dispatch(allActions.counterActions.increment())}>Increase Counter</button>
      {/*Кнопка уменьшения значения счетчика на единицу */}
      {/*Button to decrease the counter value by unit */}
      <button onClick={() => dispatch(allActions.counterActions.decrement())}>Decrease Counter</button>
    </div>
  )
}

export default App;