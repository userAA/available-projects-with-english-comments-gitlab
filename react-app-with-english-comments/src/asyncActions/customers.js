import { addManyCustomerAction } from "../store/customerReducer"

//exporting request function for downloading of set customers from server
export const fetchCustomers = () => 
{
    return function(dispatch) 
    {
        //we making up request for downloading of set customers from server 
        fetch("https://jsonplaceholder.typicode.com/users")
        //we fixing received result
        .then(response => response.json())
        //we sending the received result into reducer of customers
        .then(json => dispatch(addManyCustomerAction(json)))
    }
}