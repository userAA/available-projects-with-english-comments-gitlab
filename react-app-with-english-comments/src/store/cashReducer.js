//initial state of cash
const defaultState = 
{
    cash: 5
}

export const cashReducer = (state = defaultState, action) => 
{
    switch (action.type) 
    {
        //according to this label we increasing the value of cash on value action.payload
        //and received state we sending into storage store
        case "ADD_CASH":
            return {...state, cash: state.cash + action.payload}
        //according to this label we decreasing the value of cash on value action.payload
        //and received state we sending into storage store
        case "REMOVE_CASH":
            return {...state, cash: state.cash - action.payload}
        default:
            return state;
    }
}
