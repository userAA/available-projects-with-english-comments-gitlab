import {createStore, combineReducers, applyMiddleware} from "redux";
//we finding cash reducer
import {cashReducer} from "./cashReducer";
//we finding customers reducer
import {customerReducer} from "./customerReducer";
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from "redux-thunk";

//we compiling the full reducer from cash reducer and customers reducer
const rootReducer = combineReducers({
    //we setting the cash reducer in kind of cash
    cash: cashReducer,
    //we setting the customers reducer in kind of customers
    customers: customerReducer
})

//we compiling the storage store with using thunk
export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))