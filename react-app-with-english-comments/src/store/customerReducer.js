//the initial state of customers list
const defaultState = {
    customers: []
}

//adding customer label
const ADD_CUSTOMER = "ADD_CUSTOMER";
//the label of adding of big quantity customers from server
const ADD_MANY_CUSTOMERS = "ADD_MANY_CUSTOMERS";
//customer removing label from current list of customers
const REMOVE_CUSTOMER = "REMOVE_CUSTOMER";

export const customerReducer = (state = defaultState, action) => 
{
    switch (action.type) 
    {
        //according to label ADD_MANY_CUSTOMERS we creating the customers list, which is being transfered into storage store 
        case ADD_MANY_CUSTOMERS:
            return {...state, customers: [...state.customers, ...action.payload]}
        //according to label ADD_CUSTOMER we creating the customers list, which transfered into storage store
        case ADD_CUSTOMER:
            return {...state, customers: [...state.customers, action.payload]}
        //according to label REMOVE_CUSTOMER we creating the customers list, which transfered into storage store
        case REMOVE_CUSTOMER:
            return {...state, customers: state.customers.filter(customer => customer.id !== action.payload)}
        default:
            return state;
    }
}

//the request for adding into customers list of one new customer
export const addCustomerAction = (payload) => ({type: ADD_CUSTOMER, payload});
//the request for adding into customers list of downloaded customers set from server
export const addManyCustomerAction = (payload) => ({type: ADD_MANY_CUSTOMERS, payload});
//the request for removing from available customers list of individual customer with ID id 
export const removeCustomerAction = (payload) => ({type: REMOVE_CUSTOMER, payload});