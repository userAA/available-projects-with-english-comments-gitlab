//we finding hooks useDispatch and useSelector
import { useDispatch, useSelector } from 'react-redux';
import './App.css';
import { fetchCustomers } from './asyncActions/customers';
import { addCustomerAction, removeCustomerAction } from './store/customerReducer';

function App() 
{
  //we finding the component dispatch from hook useDispatch
  const dispatch   = useDispatch();
  //we finding the cash state from storage store using hook useSelector
  const cash       = useSelector(state => state.cash.cash);
  //we finding customers list from storage store using hook useSelector
  const customers  = useSelector(state => state.customers.customers);

  //increasing cash function
  const addCash = (cash) => 
  {
    dispatch({type: "ADD_CASH", payload: cash})
  }

  //decreasing cash function
  const getCash = (cash) => 
  {
    dispatch({type: "GET_CASH", payload: cash})
  }

  //adding customers function
  const addCustomer = (name) => 
  {
    //we compiling information about adding customers according to his name name and his ID id
    const customer = 
    {
      name,
      id: new Date()
    }
    //we making up request for customer adding
    dispatch(addCustomerAction(customer))
  }

  //removing customer function
  const removeCustomer = (customer) => 
  {
    //we making up request for customer removing according to his ID id
    dispatch(removeCustomerAction(customer.id))
  }

  return (
    <div className={"app"}>
      {/*We representing total balance */}
      <div style={{fontSize:"3rem", marginBottom: 10}}>Balance: {cash}</div>
      <div style={{display: "flex"}}>
        {/*Adding cash button */}
        <button onClick={() => addCash(Number(prompt()))}>Adding cash</button>
        {/*Withdrawal button from the account */}
        <button onClick={() => getCash(Number(prompt()))}>Withdraw from the account</button>
        {/*New customer adding button */}
        <button onClick={() => addCustomer(prompt())}>Add customer</button>
        {/*The button for receiving of all customers from database */}
        <button onClick={() => dispatch(fetchCustomers())}>Get customers from the database</button>
      </div>
      {customers.length > 0 ? 
        <div>
          {customers.map(customer => 
            //we outputing the name of every customer with possibility of his removing from current list of customers
            <div onClick={() => removeCustomer(customer)} style={{ fontSize: "2rem", border: "1px solid black", padding: "1px",marginTop: 5}}>
              {/*itself name of every customer */}
              {customer.name}
            </div>
          )}
        </div> 
        :
        //there are no customers left, either there is no one or all customers are deleted
        <div style={{fontSize: "2rem", marginTop: 20}}>
          There are no clients!
        </div>
      } 
    </div>
  );
}

export default App;

