import React from 'react';
import ReactDOM from 'react-dom';
import App from './App.jsx';
import { Provider } from 'react-redux';
//we finding the storage store
import {store} from "./store"

ReactDOM.render(
    //the storage store we sending into provider 
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);
