import React from 'react';
import {requestDog, requestDogSuccess, requestDogError} from './actions/actions'

//the function of downloading from server image web-address with dog
//in this function is being serving the element dispatch in which the functions of requests from actions are being wrapped
const fetchDog = (dispatch) => {
  //the downloading of image web-address from server is being begined, appropiate function we wraping into component dispatch
  dispatch(requestDog());
  return fetch('https://dog.ceo/api/breeds/image/random')
    .then(res => res.json())
    .then (
      //the downloading of image web-address from server may be successfully finished, appropiate function we wraping into component dispatch
      data => dispatch(requestDogSuccess(data)),
      //the downloading of image web-address from server may to finish with error, appropiate function we wraping into component dispatch
      err => dispatch(requestDogError(err))
    )
}

//Project component
class App extends React.Component {
  render () {
    return (
      <div>
         {/*The launching button of downloading from server the image web-address with dog*/}
        <button onClick={() => fetchDog(this.props.dispatch)}>Show Dog</button>
        {
          //from props we receiving the result about implementing of specified proccess
          this.props.loading
        ? 
          <p>Loading...</p>
        :
          //from props we receiving the result about error, which arose during implementing of specified proccess
          this.props.error 
        ? 
          <p>Error, try again</p> 
        : 
          //we representing image, web-address of which was received in result of implemented downloading
          <p><img alt="" src={this.props.url}/></p>
        }
      </div>      
    )
  }  
}

export default App;
