import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from 'react-redux';
import {createStore} from 'redux';

import {reducer} from './reducer/reducer'
import App from './App';

//we transfering the reducers into storage
const store = createStore(reducer);

const ConnectedApp = connect((state) => {
  console.log(state);
  return state;
})(App)

ReactDOM.render(
  //we fixing the storage store in profider
  <Provider store={store}>
    <ConnectedApp />
  </Provider>,
  document.getElementById('root')
);