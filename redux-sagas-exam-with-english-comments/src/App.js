import {fetchDog} from './action/action';
import React from 'react';

//Project component 
class App extends React.Component {
  render () {
    return (
      <div>
        {/*The launching button of downloading from server the image web-address with dog */}
        <button onClick={() => this.props.dispatch(fetchDog())}>Show Dog</button>
        {
          //from props we receiving the result about implementing of specified proccess
          this.props.loading
        ? 
          <p>Loading...</p>
        : 
          //from props we received te result about error, which arose during specified proccess implementing
          this.props.error 
        ? 
          <p>Error, try again</p> 
        : 
          //we representing image, web-address of which was received at result of implementing downloading
          <p><img src={this.props.url}/></p>}
      </div>
    )
  }
}

export default App;