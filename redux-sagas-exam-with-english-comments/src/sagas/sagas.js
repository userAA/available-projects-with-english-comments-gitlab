import axios from 'axios';
//we finding the sagas-effects components
import {takeLatest, put, call} from 'redux-saga/effects';
import { requestDog, requestDogSuccess, requestDogError } from '../action/action';

//the downloading function of image web-addressfrom server   
function* fetchDogAsync() {
    try 
    {
        //the downloading image web-address from server is being begined, appropriate function we wraping into component put
        yield put(requestDog());
        //we downloading data from server, which related with image web-address, appropriate request we wrapping into component call
        const data = yield call(() => 
        {
            return axios.get('https://dog.ceo/api/breeds/image/random');
        })
        //the image web-address downloading from server was succesfully implemented, 
        //the notification function about this we wraping into component put
        yield put (requestDogSuccess(data.data));
    } 
    catch (error) 
    {
        //the implementing proccess was finished with error, the notification function about this we wraping into component put 
        yield put(requestDogError());
    }
}

//saga launch control function, when the tag FETCHED_DOG  is triggered, the function fetchDogAsync is being launched
export function* watchFetchDog() 
{
    yield takeLatest('FETCHED_DOG', fetchDogAsync);
}