//the initial state of downloading proccess of image web-address from server
export const initialState = 
{
    //image web-address on server
    url:'',
    //the flag of implementing of downloading proccess of image web-address from server
    loading: false,  
    //the flag talking about, that during downloading proccess of image web-address from server an error has occured
    error: false
}

export const reducer = (state = initialState, action) => 
{
    switch (action.type) 
    {
        //the result of launching proccess of  image web-address downloading we put into storage store
        case 'REQUESTED_DOG':
            return {
                url: '',
                loading: true,
                error: false
            };
        //received image web-address action.url we putting into storage store
        case 'REQUESTED_DOG_SUCCEEDED':
            return {
                url: action.url,
                loading: false,
                error: false
            };
        //the message about error, which arizing during implementing proccess, we putting into storage store
        case 'REQUESTED_DOG_FAILED':
            return {
                url: '',
                loading: false,
                error: true
            };
        default: 
            return state;
    }
}