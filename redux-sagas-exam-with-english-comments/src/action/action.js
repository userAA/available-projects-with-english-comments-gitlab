//we begining downloading proccess of image web-address from server
export const requestDog = () => 
{
    return {type: 'REQUESTED_DOG'}
}

//we implemented the downloading proccess of image web-address from server, received web-address url we sending into store
export const requestDogSuccess = (data) => 
{
    return {type: 'REQUESTED_DOG_SUCCEEDED', url: data.message}
}

//during downloading proccess of image web-address from server an error has occured
export const requestDogError = () => 
{
    return {type: 'REQUESTED_DOG_FAILED'}
}

//activation function of the saga launch tag FETCHED_DOG
export const fetchDog = () => 
{
    return {type: 'FETCHED_DOG'}
}