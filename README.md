gitlab page https://gitlab.com/userAA/available-projects-with-english-comments-gitlab.git
gitlab comment available-projects-with-english-comments-gitlab

1. project fileupload (files uploader via server cloudinary).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    react,
    react-dom,
    react-scripts.

2. project pern-todo (the list of tasks names, with possibility of them adding, edditing and removing).
technologies used on the frontend:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts.

technologies used on the beckand:
    cors,
    dotenv,
    express,
    nodemon,
    pg,
    pg-hstore,
    sequelize.

3. project react-hook-use-selector (work hook useSelector demonstration).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux.

4. project redux-app (display a list of consumers and their cash budget, the budget can be reduced or supplies).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-devtools,
    redux-devtools-extension,
    redux-thunk.

5. project redux-exam (the image representation according to web-address on server).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux.

6. project redux-exam-sagas (the image representation according to web-address on server using sagas).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-saga.

7. project redux-exam-thunk (the image representation according to web-address on server using thunk).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-thunk.

8. project react_redux_ts_course (displaying a list of usernames and a list of task names with pagination).
technologies used:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    @types/jest,
    @types/node,
    @types/react,
    @types/react-dom,
    @types/react-redux,
    axios,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-thunk,
    typescript.